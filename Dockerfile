FROM debian:bookworm
RUN echo 'deb http://deb.debian.org/debian bookworm-backports main' > /etc/apt/sources.list.d/backports.list

# install base packages and update system
RUN  apt update -qy \
&& apt install zsh curl git wget sudo locales apt-utils tmux zip tar unzip bc python3 -qy \
&&  apt -qy full-upgrade

# Set the locale
RUN DEBIAN_FRONTEND=noninteractive TZ=Etc/UTC apt -qy install tzdata
ENV LANG C.UTF-8
ENV LC_ALL C.UTF-8
ENV DEBIAN_FRONTEND noninteractive
RUN locale-gen

# Create a user with passwordless sudo and add to sudo/docker group
ARG USERNAME=kfp
ARG USER_UID=1000
ARG USER_GID=$USER_UID
RUN groupadd --gid $USER_GID $USERNAME \
    && useradd --uid $USER_UID --gid $USER_GID -m $USERNAME \
    && echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers
RUN adduser $USERNAME sudo

# Clone bootstrap repo
WORKDIR /home/$USERNAME
USER $USERNAME
RUN git clone https://gitlab.com/frankper/bootstrap-files.git utils

# install apt packages list and cleanup 
RUN ["/bin/zsh", "-c", "./utils/scripts/bootstrap-system-debian-container.sh"]
# install docker
RUN ["/bin/zsh", "-c", "curl -sSL https://get.docker.com/ | sh"]
# install dotfiles and cli tools
RUN ["/bin/zsh", "-c", "./utils/scripts/install_omzsh.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_omz_plugins.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_fzf.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_asdf.sh"]
RUN ["/bin/zsh", "-c", "sudo ./utils/scripts/install_starship.sh --yes"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_tmux_samoskin.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/bootstrap-dotfiles.sh"]
# install Dev Tools 
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_asdf_repo.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_asdf_apps_dev.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/setup_lazy_neovim.sh"]
RUN ["/bin/zsh", "-c", "sudo ./utils/scripts/install_devbox.sh -f"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_yq.sh"]
RUN ["/bin/zsh", "-c", "./utils/scripts/install_krew.sh"]
# Add user kfp to other groups 
#RUN ["/bin/zsh", "-c", "sudo ./utils/scripts/bootstrap-kfp-user-groups.sh"]
# random tools not used 
#RUN ["/bin/zsh", "-c", "./utils/scripts/setup_pipx.sh"]
#RUN ["/bin/zsh", "-c", "./utils/scripts/install_arkade.sh"]
#RUN ["/bin/zsh", "-c", "./utils/scripts/setup_arkade.sh"]
RUN sudo adduser $USERNAME docker


ENTRYPOINT ["/bin/zsh"]
ENV USER=$USERNAME