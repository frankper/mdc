#
# ~/.bash_profile
#
export EDITOR='vim'
export HISTCONTROL=ignoreboth:erasedups

# devbox global config
eval "$(devbox global shellenv --init-hook)"