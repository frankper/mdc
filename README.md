# **LATEST VERSION v0.1.4**

Table of Contents
=================

* [<strong>LATEST VERSION v0.1.4</strong>](#latest-version-v008)
* [Table of Contents](#table-of-contents)
* [[WIP] Container with a collection of tools and some of my own dotfiles](#wip-container-with-a-collection-of-tools-and-some-of-my-own-dotfiles)
* [How to run this on your machine](#how-to-run-this-on-your-machine)
* [Notes](#notes)
* [Issues](#issues)
* [Todo](#todo)
* [<strong>Pipeline Configuration</strong>](#pipeline-configuration)
   * [Gitlab CI](#gitlab-ci)
   * [Github Actions](#github-actions)
* [<strong>Programming languages</strong>](#programming-languages)
* [Repo Tree](#repo-tree)

<!-- Created by https://github.com/ekalinin/github-markdown-toc -->

# [WIP] Container with a collection of tools and some of my own dotfiles
Used for running an isolated dev enviroment based on Debian 12 (bookworn)+backports
Please note that due to the number of installed packages and lack of any optimization this image is **larger than 1GB**

# How to run this on your machine
If you want to have DIND(docker in docker) enabled run 
```shell
docker run -v /var/run/docker.sock:/var/run/docker.sock -ti registry.gitlab.com/frankper/mdc:v0.1.4
```
**Just a word of caution: If your container gets access to docker.sock, it means it has more privileges over your docker daemon. So when used in real projects, understand the security risks, and use it**

For using the container without DIND
```shell
docker run -ti registry.gitlab.com/frankper/mdc:v0.1.4
```
# Notes
* User created for the container has passwordless sudo
# Issues 
* Large image size (>1gb)
# Todo 
* Add tests
* Add SSH,GPG,AWS,KUBECONFIG functionality
* Correct typos in README.md as I am sure there are many :) 
* Add github actions configuration
* Reduce image size if possible 
* Reduce container image build times (currently >35 minutes)

# **Pipeline Configuration**
## Gitlab CI 
Repo contains a gitlab pipeline [file](https://gitlab.com/frankper/mdc/-/blob/main/.gitlab-ci.yml) to build and publish with every push to main
* an image with "commit sha" tag 
* an image with "latest" tag
* an image with "git tag" tag ,if pressent
## Github Actions
WIP (at the moment blocked due to image size limitations on the [free](https://github.com/features/packages#pricing) github registry)

# **Programming languages**

Refer to my [bootstrap-files repo](https://gitlab.com/frankper/bootstrap-files)

Apt packages installed are [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/system_packages_to_install_debian_container.txt?ref_type=heads)
Asdf packages installed are [here](https://gitlab.com/frankper/bootstrap-files/-/blob/main/scripts/setup_asdf_apps_dev.sh?ref_type=heads)

# Repo Tree
```
├── .dockerignore
├── .gitignore
├── .gitlab-ci.yml
├── Dockerfile
├── LICENSE
└── README.md
```